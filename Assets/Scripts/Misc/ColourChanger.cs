﻿using UnityEngine;
using System.Collections;

public class ColourChanger : MonoBehaviour
{


    [SerializeField]
    Color HoldColour;
    [SerializeField]
    int squareNum;
    SpriteRenderer SR;
    Color originalColour;
    // Use this for initialization
    void Start()
    {
        SR = GetComponent<SpriteRenderer>();
        originalColour = SR.color;
        //Debug.Log(originalColour);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Hold" + squareNum))
        {
            SR.color = HoldColour;
        }
        else
        {
            SR.color = originalColour;
        }
    }
}
