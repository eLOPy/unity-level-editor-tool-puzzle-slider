﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    //this class will handle inputs and interface 
    //with the grid to represent the player in the game
    [SerializeField]
    GameObject PlayerPrefab;
    [SerializeField]
    public GameObject TargetPrefab;
    [SerializeField]
    Color HoldColour;
    [SerializeField]
    public Color[] PColours;

    GameData gd;
    bool[] circlesBeingHeld;
    void Start()
    {
        gd = GameData.GD;
    }

    public void Init()
    {
        Start();
         GameData.GD.PlayerPositions = new gridPos[gd.NumPlayerSquares];
         GameData.GD.PlayerCircles = new GameObject[gd.NumPlayerSquares];
        circlesBeingHeld = new bool[gd.NumPlayerSquares];
        for (int i = 0; i < gd.NumPlayerSquares; i++)
        {
            gd.PlayerPositions[i] = new gridPos(0, 0);
            gd.PlayerCircles[i] = Instantiate(PlayerPrefab) as GameObject;
            gd.PlayerCircles[i].transform.SetParent(transform);

            //gd.TargetPos[i] = new gridPos(0, 0);
            gd.Targets.Add((Instantiate(TargetPrefab) as GameObject).GetComponent<Target>()); 
            gd.Targets[i].transform.SetParent(transform);
            gd.Targets[i].GetComponent<SpriteRenderer>().color = PColours[i];

        }


    }

    // Update is called once per frame
    void Update()
    {
        UpdateHolds(gd.NumPlayerSquares);
        UpdateSquareGridPos(gd.PlayerPositions, gd.NumPlayerSquares);
        RepositionPrefabs();
    }

    private void RepositionPrefabs()
    {
        for (int i = 0; i < gd.NumPlayerSquares; i++)
        {
            Vector3 newPlayerPos;


            newPlayerPos = gd.GridToWorldCoordinates(gd.PlayerPositions[i]._x, gd.PlayerPositions[i]._y);


            gd.PlayerCircles[i].transform.localPosition = newPlayerPos;


        }
    }

    private void UpdateSquareGridPos(gridPos[] colouredSquarePos, int numColouredSquares)
    {

        gridPos movement = new gridPos(0, 0);
        if (Input.GetButtonUp("Up"))
        {
            movement._y = 1;
        }
        if (Input.GetButtonUp("Down"))
        {
            movement._y = -1;
        }
        if (Input.GetButtonUp("Left"))
        {
            movement._x = -1;
        }
        if (Input.GetButtonUp("Right"))
        {
            movement._x = 1;
        }

        if (movement != new gridPos(0,0))
        {
            gd.MoveCount++;
            gd.IsTimeOn = true;
        }

        for (int i = 0; i < numColouredSquares; i++)
        {
            if (!circlesBeingHeld[i])
            {
                if (isValidMovement(movement+ colouredSquarePos[i], i, movement))
                {
                    
                    colouredSquarePos[i] += movement;
                }
            }
        }
    }

    private bool isValidMovement(gridPos newGPos, int squareNum, gridPos movement)
    {
        
        if (!isInBounds(newGPos))
            return false;

        //if the square exists
        if (gd[newGPos._x, newGPos._y] == null)
        {
            return false;
        }
        for (int i = 0; i < gd.NumPlayerSquares; i++)
        {

            //if it's not this one 
            if (i == squareNum)
                continue;
            //check we aren't going to move into another player who is stuck in place
            //and the other player isn't trapped on the edge
            if (!isPlayerOnEdge(i, movement) && !circlesBeingHeld[i])
                continue;
           
            //make sure the new pos isn't the same as it's pos
            if (newGPos == gd.PlayerPositions[i])
                return false;
        }
        return true;
    }

    private bool isInBounds(gridPos newGPos)
    {
        if (newGPos._x < 0 || newGPos._x > gd.Width-1 )
            return false;
        if (newGPos._y < 0 || newGPos._y > gd.Height-1)
            return false;
        return true;
    }

    private bool isPlayerOnEdge(int i, gridPos movement)
    {
        gridPos pos = gd.PlayerPositions[i];
        if (movement._x != 0)
        {
            if (pos._x == 0 || pos._x == gd.Width-1)
            {
                return true;
            }

        }

        if (movement._y != 0)
        {
            if (pos._y == 0 || pos._y == gd.Height-1)
            {
                return true;
            }
        }        
        return false;
    }   

    private void UpdateHolds(int numColouredSquares)
    {
        for (int i = 0; i < numColouredSquares; i++)
        {
            if (Input.GetButton("Hold" +(i+1)))
            {
                circlesBeingHeld[i] = true;
                changeCircleColor(gd.PlayerCircles[i], HoldColour);
            }
            else
            {
                circlesBeingHeld[i] = false;
                changeCircleColor(gd.PlayerCircles[i], PColours[i]);
            }
        }
    }

    private void changeCircleColor(GameObject circle, Color colour)
    {
        circle.GetComponent<SpriteRenderer>().color = colour;    
    }

    public bool IsAnythingBeingHeld 
    {
        get
        {
            for (int i = 0; i < gd.NumPlayerSquares; i++)
            {
                if (circlesBeingHeld[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

}
