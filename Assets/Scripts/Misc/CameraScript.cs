﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
    bool isStartScreen;

	// Use this for initialization
	void Start () {
        GameData gd =  GameData.GD;
        isStartScreen = gd == null;
        if (!isStartScreen)
        {
            Vector3 pos = gd.gameObject.transform.position;
            pos += new Vector3((GameData.GD.Width + 1) / 2.0f,
                                (GameData.GD.Height + 1) / 2.0f,
                                -10);
            GetComponent<Camera>().transform.position = (pos); 
        }
        else
        {
            GetComponent<Camera>().backgroundColor = Color.black;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
