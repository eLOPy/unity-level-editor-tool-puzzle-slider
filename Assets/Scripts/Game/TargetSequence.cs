﻿using UnityEngine;
using System;


[Serializable]
public class TargetSequence : ScriptableObject
{
    [SerializeField]
    private TargetFrame[] _frames;
    [SerializeField]
    private Difficulty _difficulty;


    public void Init(TargetFrame[] f, Difficulty d)
    {
        _frames = f;
        _difficulty = d;
    }
    public TargetFrame GetFrame(int i )
    {
        int t = i % _frames.Length; //avoid division by 0
        return _frames[t];
    }

    public Difficulty Difficulty { get { return _difficulty; } }
    public int Frames { get { return _frames.Length; } }
}
