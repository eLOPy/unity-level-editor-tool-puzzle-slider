﻿using UnityEngine;
using System;

using System.Collections.Generic;

//this holds data of a pattern
public enum Difficulty
{
    easy,
    medium,
    hard,
    very_hard
}
[Serializable]
public class TargetFrame
{
    public Vector2[] targetPositions;
    public LevelSettings Settings;

    public void Init(Vector2[] tpos, Difficulty d, LevelSettings ls)
    {
        Settings = ls;
        targetPositions = tpos;
    }

}
