﻿using UnityEngine;
using System;

[Serializable]
public class LevelSettings : ScriptableObject {

    public int numPlayerIcons = 2;
    
    public int iWidth, iHeight;

    public int sTimeLimit;

    public int levelNo = 0;
}
