﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct gridPos
{
    public gridPos(int x, int y) { _x = x; _y = y; }

    public gridPos(Vector2 v) { _x = (int)v.x;_y = (int)v.y; }
    public static gridPos operator +(gridPos left, gridPos right)
    {
        int x, y;
        x = left._x + right._x;
        y = left._y + right._y;
        return new gridPos(x, y);
    }

    public static bool operator ==(gridPos left, gridPos right)
    {
        bool xEqual = left._x == right._x;
        bool yEqual = left._y == right._y;
        return xEqual && yEqual;

    }
    public static bool operator !=(gridPos l, gridPos r)
    {
        bool x = l._x != r._x;
        bool y = l._y != r._y;
        return x || y;
    }

    public override bool Equals(object obj)
    {
        return false;
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public int _x, _y;
}

public class GameData : MonoBehaviour
{
    [HideInInspector]
    public const float GridSize = 1.28f;

    public static GameData _gd;

    [SerializeField]
    private LevelSettings _ls;
    [SerializeField]
    public GUIUtils gui;

    [SerializeField]
    private GridSquare[] _grid;
    [SerializeField]
    private Target[] _targetLayer;

    List<GridSquare> NonNullGridSquareList;

    private readonly Color _normalColour = Color.grey;
    private readonly Color _selectedColour = Color.yellow;

    // Use this for initialization
    public static GameData GD
    {
        get
        {
            if (_gd == null)
            {
                _gd = GameObject.FindObjectOfType<GameData>();
            }
            return _gd;
        }
    }

    void Start()
    {

    }

    public void InitData(bool firstInit)
    {
        CurrentTime = 0;
        MoveCount = 0;
        IsTimeOn = false;

        if (firstInit)
        {
            Targets = new List<Target>();
            GetGridSquares();
            PlayerPositions = new gridPos[NumPlayerSquares];
            PlayerCircles = new GameObject[NumPlayerSquares];
            PlayerController p = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            if (p != null)
            {
                p.Init(); 
            }
            else
            {
                Debug.LogError("Couldn't find a player controller in the scene.");
            }
        }
    }

    public void GetTargetsInScene()
    {
        int gridSize = Width * Height;
        TargetLayer = new Target[gridSize];
        Target[] tSquares = GetComponentsInChildren<Target>();
        for (int i = 0; i < tSquares.Length; i++)
        {
            int col, row;
            col = (int)tSquares[i].gridPos.x;
            row = (int)tSquares[i].gridPos.y;
            SetTarget(col, row, tSquares[i]);

        }
    }

    public Vector2 getRandomGridPos()
    {


        int i = Random.Range(0, NonNullGridSquareList.Count - 1);


        return (NonNullGridSquareList[i].gridPos);

    }

    public Vector2 GetUnoccupiedSquarePositionAtRandom()
    {
        //get random square pos
        Vector2 randomPos = getRandomGridPos();
        //check if the square is unoccupied

        bool isUnoccupied = isSquareUnoccupied(randomPos);



        while (!isUnoccupied)
        {
            randomPos = getRandomGridPos();

            isUnoccupied = isSquareUnoccupied(randomPos);
        }

        return randomPos;
                
    }

    private bool isSquareUnoccupied(Vector2 p)
    {
        //test for a player
        bool pthere = isPlayerThere(new gridPos(p));
        //test for a target
        bool tthere = isTargetThere(p);
        return !pthere && !tthere;
    }

    ////////////////////////////////////////////////////////////////////
    ////////////////////////  Grid Query Functions  ////////////////////
    ////////////////////////////////////////////////////////////////////


    public Vector3 WorldToGridCoordinates(Vector3 point)
    {
        Vector3 gridPoint = new Vector3(
            (int)((point.x - transform.position.x) / GridSize),
            (int)((point.y - transform.position.y) / GridSize),
            0);
        return gridPoint;
    }

    public Vector3 GridToWorldCoordinates(int col, int row)
    {
        Vector3 worldPoint = new Vector3(
            (int)transform.position.x + (col * GridSize + GridSize / 2.0f),
            (int)transform.position.y + (row * GridSize + GridSize / 2.0f),
            0);
        return worldPoint;
    }

    public bool IsInsideGridBounds(Vector3 point)
    {
        float minX = transform.position.x;
        float maxX = minX + GridSize * _ls.iWidth;
        float minY = transform.position.y;
        float maxY = minY + _ls.iHeight * GridSize;
        return (point.x >= minX && point.x <= maxX &&
                point.y >= minY && point.y <= maxY);
    }

    public bool IsInsideGridBounds(int col, int row)
    {
        return (col >= 0 && col < _ls.iWidth &&
                row >= 0 && row < _ls.iHeight);
    }


    public void GetGridSquares()
    {

        int gridSize = Width * Height;
        Grid = new GridSquare[gridSize];
        GridSquare[] gSquares = GetComponentsInChildren<GridSquare>();
        NonNullGridSquareList = new List<GridSquare>();
        for (int i = 0; i < gSquares.Length; i++)
        {
            if (gSquares[i].GetComponent<Target>() != null)
            {
                continue;
            }
            int col, row;
            col = (int)gSquares[i].gridPos.x;
            row = (int)gSquares[i].gridPos.y;
            this[col, row] = gSquares[i];
            NonNullGridSquareList.Add(this[col, row]);
        }

        //if (NonNullGridSquareList.Count == 0)
        //{
        //    Debug.Log("Didn't find any Grid Squares !!");
        //}
        transform.hideFlags = HideFlags.NotEditable;
    }

    ////////////////////////////////////////////////////////////////////
    ////////////////////////  Private Functions  ///////////////////////
    ////////////////////////////////////////////////////////////////////

    private bool isNewTargetValid(Vector2 potentialPos)
    {
        return !isPlayerThere(new gridPos(potentialPos)) && !isTargetThere(potentialPos);
    }

    private bool isPlayerThere(gridPos pos)
    {
        for (int i = 0; i < NumPlayerSquares; i++)
        {
            if (pos == PlayerPositions[i])
            {
                return true;
            }
        }
        return false;
    }
    private bool isTargetThere(Vector2 pos)
    {
        for (int i = 0; i < NumPlayerSquares; i++)
        {
            if (pos == Targets[i].gridPos)
            {
                return true;
            }
        }
        return false;
    }


    ////////////////////////////////////////////////////////////////////
    ////////////////////////  GridGizmo Stuff  /////////////////////////
    ////////////////////////////////////////////////////////////////////

    private void OnDrawGizmos()
    {
        // save the colour
        Color oldColour = Gizmos.color;
        Matrix4x4 oldMatrix = Gizmos.matrix;
        //set the current draw colour to the one we want
        Gizmos.color = _normalColour;
        Gizmos.matrix = transform.localToWorldMatrix;
        // draw the grid
        if (_ls == null)
        {
            Debug.LogError("Attach a level settings to the gameData script!");
            return;
        }

        GridGizmo(_ls.iWidth, _ls.iHeight);
        GridFrameGizmo(_ls.iWidth, _ls.iHeight);
        // restore the draw colour back to the old one
        Gizmos.color = oldColour;
        Gizmos.matrix = oldMatrix;
    }

    private void OnDrawGizmosSelected()
    {
        // save the colour
        Color oldColour = Gizmos.color;
        Matrix4x4 oldMatrix = Gizmos.matrix;
        //set the current draw colour to the one we want
        Gizmos.color = _selectedColour;
        Gizmos.matrix = transform.localToWorldMatrix;
        // draw the higlighted border
        GridFrameGizmo(_ls.iWidth, _ls.iHeight);
        // restore the draw colour back to the old one
        Gizmos.color = oldColour;
        Gizmos.matrix = oldMatrix;
    }

    private void GridGizmo(int cols, int rows)
    //draws the inside grid
    {
        //draw all the columns
        for (int i = 1; i < cols; i++)
        {
            Gizmos.DrawLine(
                new Vector3(i * GridSize, 0, 0),
                new Vector3(i * GridSize, rows * GridSize, 0)
                );
        }
        //draw all the rows
        for (int i = 1; i < rows; i++)
        {
            Gizmos.DrawLine(
                new Vector3(0, i * GridSize, 0),
                new Vector3(cols * GridSize, i * GridSize, 0)
                );
        }
    }

    private void GridFrameGizmo(int cols, int rows)
    //draws the borders
    {
        //left vertical
        Gizmos.DrawLine(Vector3.zero, new Vector3(0, rows * GridSize, 0));
        //top horizontal
        Gizmos.DrawLine(Vector3.zero, new Vector3(cols * GridSize, 0, 0));
        //right vertical
        Gizmos.DrawLine(
            new Vector3(cols * GridSize, 0, 0),
            new Vector3(cols * GridSize, rows * GridSize, 0));
        //bottom horizontal
        Gizmos.DrawLine(
            new Vector3(0, rows * GridSize, 0),
            new Vector3(cols * GridSize, rows * GridSize, 0));
    }


    public Target GetTarget(int i , int j)
    {
        return _targetLayer[i + j * Width];
    }

    public void SetTarget(int i, int j, Target t)
    {
        _targetLayer[i + j * Width] = t;
    }
    public void ClearTargets()
    {
        foreach (Target t in TargetLayer)
        {
            if (t != null)
            {
                DestroyImmediate(t.gameObject);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    ////////////////////////  Properties  //////////////////////////////
    ////////////////////////////////////////////////////////////////////

    public GridSquare this[int i, int j]
    {
        get { return _grid[i + j * Width]; }
        set { _grid[i + j * Width] = value; }
    }
    public GridSquare[] Grid
    {
        get { return _grid; }
        set { _grid = value; }
    }
    public LevelSettings Settings
    {
        get { return _ls; }
        set { _ls = value; }
    }
    public Target[] TargetLayer
    {
        get { return _targetLayer; }
        set { _targetLayer = value; }
    }
    public gridPos[] PlayerPositions { get; set; }
    public Color[] Colours { get; set; }
    public GameObject[] PlayerCircles { get; set; }
    public List<Target> Targets { get; set; }
    //public gridPos[] TargetPos { get; set; }
    public int NumPlayerSquares { get { return _ls.numPlayerIcons; } }
    public int Width { get { return _ls.iWidth; } }
    public int Height { get { return _ls.iHeight; } }
    public int Score { get; set; }
    public float CurrentTime { get; set; }
    public bool IsTimeOn { get; set; }
    public int TimeLimit { get { return _ls.sTimeLimit; } }
    public float TimeLeft { get { return _ls.sTimeLimit - CurrentTime; } }
    public bool HasTimeFinished { get; set; }
    public int MoveCount { get; set; }

    public int NumGridSquaresInScene { get { return NonNullGridSquareList.Count; } }

}
