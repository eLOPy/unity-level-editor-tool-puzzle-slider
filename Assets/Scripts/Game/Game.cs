﻿using UnityEngine;
using UnityEditor;
using LevelMaker;
using System.Collections;

public class Game : MonoBehaviour
{
    GameData gd;

    private PatternManager _patternMgr;
    // Use this for initialization
    void Start() //make sure to init the GameData
    {
        //cache this cause 
        gd = GameData.GD;
        _patternMgr = transform.parent.GetComponentInChildren<PatternManager>();
        if (_patternMgr == null)
        {
            Debug.Log("Couldn't find pattern mgr");
        }
        gd.InitData(true);
        bool ok = MakeSureSomeoneMadeALevel();
        
        InitGame();

        
        
    }

    private bool MakeSureSomeoneMadeALevel()
    {
        bool reasonableAmountOfGridSquares = GameData.GD.NumGridSquaresInScene > 4;
        if (!reasonableAmountOfGridSquares)
        {
            LevelMaker.EditorHelpers.DisplayLevelMakerError(
                "You probably dont have enough squares in the scene for a playable level.\nUse the paint button and the Palette(F10) in the level maker to put more Grid Squares in the level.", 
                "Cool i'll do that."
                );
            
        }
        return reasonableAmountOfGridSquares;
    }

    void InitGame()
    {
        //Spawn the players
        _patternMgr.GetInitTarget();
        SpawPlayers();
        // Start the time
        gd.HasTimeFinished = false;
        StartCoroutine(UpdateTime());
    }

    private void SpawPlayers()
    {
        for (int i = 0; i < gd.NumPlayerSquares; i++)
        {

            gd.PlayerPositions[i] = new gridPos(gd.GetUnoccupiedSquarePositionAtRandom());
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckForTimeOver();
        UpdateTargetPrefabs();

        //if all player poses are at their targets then increase score and get new targets
        if (allAtTargets())
        {
            _patternMgr.GetNewTargets();
            gd.IsTimeOn = false;
            if (!gd.HasTimeFinished)
            {
                PlayerController p = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
                if (p.IsAnythingBeingHeld)
                {
                    gd.Score += 10;
                    gd.gui.NewPopUp("+10");
                    //Debug.Log("Score 10!");
                }
                else
                {
                    gd.Score += 50;
                    gd.gui.NewPopUp("+50");
                    //Debug.Log("Score 50!");
                }
            }
        }
    }

    private void UpdateTargetPrefabs()
    {
    }

    private void CheckForTimeOver()
    {
        if (gd.CurrentTime >= gd.TimeLimit && !gd.HasTimeFinished)
        {
            gd.HasTimeFinished = true;
            AudioSource AS = GetComponent<AudioSource>();

            AS.clip = (AudioClip)Resources.Load("Audio/Finish");
            AS.loop = true;
            AS.Play();
        }
    }



    private bool allAtTargets()
    {
        for (int i = 0; i < gd.NumPlayerSquares; i++)
        {
            if (new gridPos(gd.Targets[i].gridPos) != gd.PlayerPositions[i])
            {
                return false;
            }
        }
        return true;
    }

    public void Restart()
    {
        gd.InitData(false);
        InitGame();
    }

    IEnumerator UpdateTime()
    {
        AudioSource AS = GetComponent<AudioSource>();
        AS.clip = (AudioClip)Resources.Load("Audio/TimeBlip");
        while (!gd.HasTimeFinished)
        {
            yield return null;
            if (gd.IsTimeOn)
            {
                gd.CurrentTime += Time.deltaTime;
                if (0.05 >= gd.CurrentTime % 1)
                {
                    AS.Play();
                }
            }
        }



    }
}
