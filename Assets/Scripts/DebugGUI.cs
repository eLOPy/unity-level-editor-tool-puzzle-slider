﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class DebugGUI : MonoBehaviour
{

    public Text Difficulty;
    public Text Sequence;
    private PatternManager pMgr;
    // Use this for initialization
    void Start()
    {
        pMgr = GameObject.FindGameObjectWithTag("Difficulty").GetComponent<PatternManager>();
    }

    // Update is called once per frame
    void Update()
    {

        UpdateText();
    }

    private void UpdateText()
    {
        string fDiff, fSeq;
        fDiff = string.Format("Current Difficulty: {0}", pMgr.CurrentDifficulty.ToString());
        fSeq = string.Format("Current Sequence: {0}, ", pMgr.CurrentSequence);
        Difficulty.text = fDiff;
        Sequence.text = fSeq;
    }
}
