﻿using UnityEngine;
using System.Collections;

public class GridStage : MonoBehaviour
{

    public GameObject EmptyGridSquare;


    private GameObject[] m_grid;
    private GameData gd;

    // Use this for initialization

    void Start()
    {
        gd = GameData.GD;
        ResetGrid(gd.Width, gd.Height);
    }

    private void ResetGrid(int width, int height)
    {
        m_grid = new GameObject[width * height];


        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int i = row * width + col;
                Vector3 pos = new Vector3(col, row, 0);                     //the square's grid position
                pos *= GameData.GridSize;                                  //spaces out the grid squares
                m_grid[i] = Instantiate(EmptyGridSquare, pos, Quaternion.identity) as GameObject;
                m_grid[i].transform.SetParent( transform,false);
                
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void WipeGridColour()
    {
        for (int i = 0; i < m_grid.Length; i++)
        {
            m_grid[i] = EmptyGridSquare;
        }
    }

    public void SetSquare(int index, Color colour)
    {
   
    }
   
}
