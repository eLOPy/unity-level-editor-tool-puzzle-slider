﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


public class PatternManager : MonoBehaviour
{
    public AnimationCurve _difficultyCurve = new AnimationCurve();
    //Target Icon
    //public GameObject targetPrefab;

    [SerializeField]
    public List<TargetSequence> _sequences;
    private int anim_frame_index = 0;
    private float difficulty_time = 0;
    private Difficulty _currentDifficulty;
    private System.Random random = new System.Random();
    private TargetSequence _currentAnimation;
    // Use this for initialization
    void Start()
    {
    }

    private void MakeSureUserHasAddedSomeSequences()
    {
        bool anySequencesNull = false;
        foreach (var s in _sequences)
        {
            if (s == null)
            {
                anySequencesNull = true;
            }
        }
        if (_sequences.Count == 0 || anySequencesNull)
        {
            LevelMaker.EditorHelpers.DisplayLevelMakerError(
               "You dont have any target sequences! You can make some by clicking the Draw_Target_Sequence button in the Level Maker.",
               "Cool i'll do that."
               );
        }
    }

    // Update is called once per frame
    void Update()
    {
        difficulty_time = GameData.GD.CurrentTime;

        Difficulty d =  EvaluateCurrentDifficulty();

        if(_currentDifficulty != d)
        {
            Debug.LogFormat("Difficulty changed to {0}", d.ToString());
            _currentDifficulty = d;
        }

    }

    void OnGUI()
    {
        //Debug.Log(string.Format("Difficulty: {0}", _currentDifficulty.ToString()));
    }

    private Difficulty EvaluateCurrentDifficulty()
    {
        float time_limit = GameData.GD.Settings.sTimeLimit;
        float f = _difficultyCurve.Evaluate(difficulty_time/ time_limit);
        f *= 4;
        
        return (Difficulty)f;
    }

    public void GetNewTarget(int i)
    {
        GameData.GD.Targets[i].gridPos = GameData.GD.GetUnoccupiedSquarePositionAtRandom();
    }

    public void GetNextTargetFromAnim()
    {
        if (anim_frame_index >= _currentAnimation.Frames)
        {
            GetDiffAppropriateAnimationatRandom();
        }

        TargetFrame tp = _currentAnimation.GetFrame(anim_frame_index);
        GameData.GD.Targets[0].Pos = (tp.targetPositions[0]);
        GameData.GD.Targets[1].Pos = (tp.targetPositions[1]);

        anim_frame_index++;

    }

    private void GetDiffAppropriateAnimationatRandom()
    {
        anim_frame_index = 0;
        Difficulty tempcd = _currentDifficulty;
        List<TargetSequence> diffGroup = GetDifficultyGroup(tempcd);
        Enumerable.Range(0, GameData.GD.Settings.numPlayerIcons);
        int i = random.Next(diffGroup.Count);
        _currentAnimation = diffGroup[i];
    }

    public void GetNewTargets()
    {
        GetNextTargetFromAnim();
    }

    public void GetInitTarget()
    {
        MakeSureUserHasAddedSomeSequences();
        GetDiffAppropriateAnimationatRandom();
        GetNextTargetFromAnim();
    }

    public List<TargetSequence> GetDifficultyGroup(Difficulty cd)
    {
        List<TargetSequence> diffgroup = new List<TargetSequence>();
        for (int i = 0; i < _sequences.Count; i++)
        {
            if (_sequences[i].Difficulty == cd)
            {
                diffgroup.Add(_sequences[i]);
            }
        }

        if (diffgroup.Count == 0)
        {
            if (cd == Difficulty.easy)
            {
                return null;
            }
            Difficulty easiercd = --cd;
            return GetDifficultyGroup(easiercd);
        }
        return diffgroup;
    }

    public List<TargetSequence> Sequences { get { return _sequences; } set { _sequences = value; } }

    public Difficulty CurrentDifficulty { get { return _currentDifficulty; } }
    public string CurrentSequence { get { return _currentAnimation.name; } }
}
