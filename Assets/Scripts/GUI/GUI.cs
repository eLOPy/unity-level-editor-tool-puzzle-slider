﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace SlidingPuzzle
{
    public class GUI : MonoBehaviour
    {
        public GameData gd;
        public Text txtTime;
        public Text txtScore;
        public Text txtMoveCount;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            updateText();
        }

        private void updateText()
        {
            updateTimeTxt((int)gd.TimeLeft);
            updateScoreTxt(gd.Score);
            updateMoveCount(gd.MoveCount);
        }

        private void updateMoveCount(int p)
        {
            txtMoveCount.text = "Moves: " + p;
        }

        private void updateScoreTxt(int score)
        {
            txtScore.text = "Score: " + score;
        }

        private void updateTimeTxt(int seconds)
        {
            int minutes = seconds / 60;
            int secs = seconds % 60;
            txtTime.text = getFormattedTime(minutes, secs);
        }

        private string getFormattedTime(int minutes, int secs)
        {
            string strMins, strSecs, res;

            if (minutes < 10)
                strMins = "0" + minutes;
            else
                strMins = "" + minutes;

            if (secs < 10)
                strSecs = "0" + secs;
            else
                strSecs = "" + secs;

            res = strMins + ":" + strSecs;
            return res;
        }
    } 
}
