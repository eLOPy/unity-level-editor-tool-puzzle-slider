﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIUtils : MonoBehaviour
{
    [SerializeField]
    GameObject pPopUp;
    public void NewPopUp(string text)
    {
        GameObject canvas = GameObject.FindWithTag("GUICanvas");
        GameObject PopUp = GameObject.Instantiate(pPopUp) as GameObject;
        PopUp.transform.SetParent(canvas.transform, false);
        Text t = PopUp.GetComponent<Text>();
        t.text = text;


  
    }

}
