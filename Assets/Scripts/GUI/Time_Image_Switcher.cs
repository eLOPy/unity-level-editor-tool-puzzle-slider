﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class Time_Image_Switcher : MonoBehaviour
{

    private Image image;
    [SerializeField]
    Sprite play = null, pause = null, stop = null;
    // Use this for initialization
    void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {

        if (!GameData.GD.HasTimeFinished)
        {
            if (GameData.GD.IsTimeOn)
            {
                image.sprite = play;
            }
            else
            {
                image.sprite = pause;
            }
        }
        else
        {
            Button b = GetComponent<Button>();
            b.interactable = true;
            image.sprite = stop;
        }
    }
}
