﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIFadeOut : MonoBehaviour {
    [SerializeField]
    float fDuration;
    Color end;
    Text myText;
    // Use this for initialization
	void Start () 
    {
        myText = GetComponent<Text>();
        end = myText.color;
        end.a = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<Text>().color.a >= 0.1)
        {
            myText.color = Color.Lerp(myText.color, end, Time.deltaTime/fDuration);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
	}

    
}
