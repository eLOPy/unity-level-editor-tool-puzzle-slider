﻿using UnityEngine;


[ExecuteInEditMode]
public class SnapToGridTest : MonoBehaviour
{

    // Update is called once per frame
    private void Update()
    {
        Vector3 gridCoord = GameData.GD.WorldToGridCoordinates
            (transform.position);
        transform.position = GameData.GD.GridToWorldCoordinates(
            (int)gridCoord.x, (int)gridCoord.y);
    }

    private void OnDrawGizmos()
    {
        Color oldColour = Gizmos.color;
        Gizmos.color = (GameData.GD.IsInsideGridBounds(transform.position)) ?
                        Gizmos.color = Color.green :
                        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, Vector3.one * GameData.GridSize);

        Gizmos.color = oldColour;
    }
}

