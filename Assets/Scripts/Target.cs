﻿using UnityEngine;
using System.Collections;

public class Target : GridSquare
{
    public int playerNo;                //the player this target is for
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public Vector2 Pos
    {
        get { return gridPos; }
        set
        {
            gridPos = value;
            transform.position = GameData.GD.GridToWorldCoordinates((int)value.x, (int)value.y);
        }
    }
}
