﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace LevelMaker
{
    public static class EditorHelpers
    {
        public static void DisplayLevelMakerError(string dialogue, string ok)
        {
            EditorUtility.DisplayDialog("Level Maker Error",
                dialogue,
                ok);

            EditorApplication.Beep();
            EditorApplication.isPlaying = false;
            Debug.DebugBreak();
            Debug.Break();
        }

    }
}
