﻿using UnityEngine;
using System.Collections;

namespace LevelMaker
{
    public class PaletteItem : MonoBehaviour
    {


#if UNITY_EDITOR
        public enum Category
        {
            GridSquares,
            PatternSquares,
            Misc
        }

        public Category category = Category.Misc;
        public string itemName = "";

        public Object inspectedScript;

#endif


    }
}
