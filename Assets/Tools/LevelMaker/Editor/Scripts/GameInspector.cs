﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace LevelMaker
{
    [CustomEditor(typeof(GameData))]
    public class LevelInspector : Editor
    {
        public enum Mode
        {
            View,
            Paint,
            Edit,
            Erase,
            Draw_Target_Sequence
        }

        private Mode _selectedMode;
        private Mode _currentMode;

        private int _originalPosX, _originalPosY;

        private int _targetIndex = -1;

        private Difficulty _sequenceDifficulty = (Difficulty) (-1);

        private bool _showDifficultySelector = false;

        private PaletteItem _itemSelected;
        private PaletteItem _itemInspected;
        private Texture2D _itemPreview;
        private GridSquare _pieceSelected;
        //private List<Target> _targetList;
        private GameData _myTarget;
        [SerializeField]
        private List<TargetFrame> _sequencePreview;
        private List<GUIContent> _sequenceGUIPreviews;
        private void OnEnable()
        {
            //Debug.Log("OnEnable Was called...");
            _myTarget = (GameData)target;
            InitLevel();
            //ResetResizeValues();
            SubscribeEvents();
            //InitStyes();
            if (_sequencePreview == null)
            {
                _sequencePreview = new List<TargetFrame>();
            }
            if (_sequenceGUIPreviews == null)
            {
                _sequenceGUIPreviews = new List<GUIContent>();
            }
        }
        private void OnDisable()
        {
            // Debug.Log("OnDisable was called...");
            UnSubscribeEvents();
            _sequencePreview.Clear();
            _myTarget.ClearTargets();

        }

        private void SubscribeEvents()
        {
            PaletteWindow.ItemSelectedEvent += new PaletteWindow.itemSelectedDelegate(UpdateCurrentPieceInstance);
        }


        private void UnSubscribeEvents()
        {
            PaletteWindow.ItemSelectedEvent -= new PaletteWindow.itemSelectedDelegate(UpdateCurrentPieceInstance);

        }

        private void OnDestroy()
        {
          //  Debug.Log("OnDestroy was called...");
        }

        public override void OnInspectorGUI()
        {
            DrawLevelData();
            DrawPieceSelectedGUI();
            //DrawPieceEditor();

            if (UnityEngine.GUI.changed)
            {
                EditorUtility.SetDirty(_myTarget);
            }
        }

        private void DrawPieceEditor()
        {
            if (_itemInspected != null)
            {
                if (_itemInspected.inspectedScript != null)
                {
                    Editor.CreateEditor(_itemInspected.inspectedScript).OnInspectorGUI();
                }
            }
        }

        void OnSceneGUI()
        {
            DrawModeGUI();
            if (_selectedMode == Mode.Draw_Target_Sequence)
            {
                DrawPatternGUI();
            }
            ModeHandler();
            EventHandler();
        }

        private void DrawPatternGUI()
        {
            Handles.BeginGUI();

            Rect PatternRect = new Rect(10, Screen.height - 100, Screen.width - 250, 40);
            GUILayout.BeginArea(PatternRect);
            GUILayout.BeginHorizontal();
            DrawClearPatternButton();
            DrawPlayerSelectGUI();
            DrawAddSequenceToPreview();
            DrawExportButtonGUI();

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
            Rect PreviewRect = new Rect(Screen.width - 110, 10, 70, Screen.height-60);

            GUILayout.BeginArea(PreviewRect);
            DrawSequencePreview();
            GUILayout.EndArea();
            if (_showDifficultySelector && _sequencePreview.Count > 0)
            {
                DrawDifficultySelector();
            }

            Handles.EndGUI();
        }
        private void DrawSequencePreview()
        {
            

            GUIStyle guiStyle = new GUIStyle(UnityEngine.GUI.skin.label);
            guiStyle.alignment = TextAnchor.LowerCenter;
            guiStyle.imagePosition = ImagePosition.ImageAbove;
            guiStyle.fixedWidth = 64;
            guiStyle.fixedHeight = 64;
            GUILayout.BeginVertical("box");
            GUIStyle style = new GUIStyle(EditorStyles.miniLabel);
            style.wordWrap = true;
            GUILayout.Label("Sequence Frames", style);
            //style.wordWrap = false;

            GUILayout.SelectionGrid(-1, _sequenceGUIPreviews.ToArray(), 1, guiStyle);
            GUILayout.EndVertical();

        }

        private Texture2D GenerateAFramePreview(TargetFrame tf)
        {
            int blockSize = 16;
            Texture2D preview = new Texture2D(GameData.GD.Width * blockSize , GameData.GD.Height * blockSize);
            //Color[] black = new Color[GameData.GD.Width * blockSize * GameData.GD.Height * blockSize];

            //for(int i =0 ; i < GameData.GD.Width * blockSize * GameData.GD.Height * blockSize; ++i)
            //{
            //    black[i] = Color.black;
            //}
            //preview.SetPixels(0,0,GameData.GD.Width * blockSize, GameData.GD.Height * blockSize,black);
            // draw the red block
            Color[] red = new Color[blockSize * blockSize];

            for(int i =0 ; i < blockSize * blockSize; ++i)
            {
                red[i] = Color.red;
            }

            preview.SetPixels((int)tf.targetPositions[0].x * blockSize, (int)tf.targetPositions[0].y * blockSize, 
                                 blockSize,  blockSize, red);


            //draw the blue block
            Color[] blue = new Color[blockSize * blockSize];

            for (int i = 0; i < blockSize * blockSize; ++i)
            {
                blue[i] = Color.blue;
            }

            preview.SetPixels((int)tf.targetPositions[1].x * blockSize, (int)tf.targetPositions[1].y * blockSize,
                                blockSize, blockSize, blue);

            return preview;
        }

       static bool sButton = false;
        private void DrawAddSequenceToPreview()
        {
            if (GUILayout.Button(new GUIContent("Add Pattern Frame To Sequence",
                "Finds all the targets in the scene and adds them to the Working Sequence"), 
                GUILayout.ExpandHeight(true)) )
            {
                AddPatternToPreview();
                
            }

        }

        private void AddPatternToPreview()
        {
            TargetFrame newTF = new TargetFrame();
            Vector2[] targetPositions = new Vector2[GameData.GD.NumPlayerSquares];
            //Fill out the assets
            //get a sorted list
            List<Target> targetList = GameData.GD.TargetLayer.ToList();
            targetList.RemoveAll(item => item == null);

            List<Target> temp = targetList.OrderBy(o => o.playerNo).ToList();

            if (temp.Count < 2)
            {
                Debug.LogWarning("There are not enough targets to make a frame.");
            }
            else
            {
                if (_sequencePreview.Count < 5)
                {
                    for (int i = 0; i < GameData.GD.NumPlayerSquares; i++)
                    {
                        targetPositions[i] = temp[i].gridPos;
                    }

                    newTF.Init(targetPositions, _sequenceDifficulty, GameData.GD.Settings);

                    _sequencePreview.Add(newTF);
                    //do twice because it works
                    Texture2D pre = GenerateAFramePreview(newTF);
                    pre = GenerateAFramePreview(newTF);

                    _sequenceGUIPreviews.Add(new GUIContent(pre, "frame " + _sequencePreview.Count.ToString()));
                    _myTarget.ClearTargets();
                }
                else
                {
                    Debug.LogWarning("There are too many frames (max 5).");                    
                }
                   
            }

        }

        private void DrawClearPatternButton()
        {


            Color oldback = GUI.backgroundColor;
            Color oldtext = GUI.backgroundColor;

            GUI.backgroundColor = new Color(0.6f,0.0f,0.0f);
            GUI.contentColor = new Color(1,1,1);

            if (GUILayout.Button(new GUIContent("Clear The Sequence", "Clears all targets"), GUILayout.ExpandHeight(true)))
            {
                _myTarget.ClearTargets();
                _sequencePreview.Clear();
                _sequenceGUIPreviews.Clear();
            }

            GUI.backgroundColor = oldback;
            GUI.contentColor = oldtext;

        }

        private void DrawDifficultySelector()
        {
            Rect DifficultyRect = new Rect(Screen.width - 230, Screen.height - 260, 100, 200);
            GUILayout.BeginArea(DifficultyRect);
            List<Difficulty> difficultyModes = EditorUtils.GetListFromEnum<Difficulty>();
            List<string> diffLabels = new List<string>();
            foreach (Difficulty diffsetting in difficultyModes)
                diffLabels.Add(diffsetting.ToString());

            _sequenceDifficulty = (Difficulty)GUILayout.SelectionGrid((int)_sequenceDifficulty, diffLabels.ToArray(), 1, GUILayout.ExpandHeight(true));

            if ((int)_sequenceDifficulty > -1)
            {
                ExportPattern();
                _showDifficultySelector = false;
            }

            GUILayout.EndArea();


        }

        private void DrawExportButtonGUI()
        {
            if (GUILayout.Button(new GUIContent("Save Sequence To Level", "Saves the current sequence and adds it to the level."), GUILayout.ExpandHeight(true)))
            {

                _showDifficultySelector = true;
            }
        }

        private void DrawPlayerSelectGUI()
        {
            GameObject targetPrefab = Resources.Load("Prefabs/Game/TargetSquare") as GameObject;

            if (targetPrefab != null)
            {
                //List<string> pnumLabels = new List<string>();
                List<GUIContent> pnumLabels = new List<GUIContent>();
                
                foreach (int pnum in Enumerable.Range(0, GameData.GD.Settings.numPlayerIcons))
                    pnumLabels.Add(new GUIContent("Player " + pnum.ToString()));

                _targetIndex = GUILayout.Toolbar(_targetIndex, pnumLabels.ToArray(), GUI.skin.button, GUILayout.ExpandHeight(true));

                PaletteItem t = targetPrefab.GetComponent<PaletteItem>();
                Texture2D tPreview = AssetPreview.GetAssetPreview(targetPrefab);
                UpdateCurrentPieceInstance(t, tPreview);
            }
            else
            {
                Debug.LogWarning("Couldn't find the target square prefab!");
            }
        }

        private void ExportPattern()
        {
            TargetSequence newTS;
            string path = EditorUtility.SaveFilePanelInProject(
                "New Target Sequence",
                "",
                "asset",
                "Select a destination for this target Sequence",
                "TargetPatterns"
                );
            if (path != "")
            {
                newTS = EditorUtils.CreateAsset<TargetSequence>(path);
                //Fill out the assets
                //get a sorted list
                newTS.Init(_sequencePreview.ToArray(), _sequenceDifficulty);

                EditorUtility.SetDirty(newTS);

                PatternManager p = GameObject.FindGameObjectWithTag("Difficulty").GetComponent<PatternManager>();

                p.Sequences.Add(newTS);
                EditorUtility.SetDirty(p);
                
                _sequencePreview.Clear();
                _sequenceGUIPreviews.Clear();
                EditorUtility.DisplayDialog("Level Maker", "Sequence was saved and has been added to the current level.", "Shweet");
            }

            AssetDatabase.SaveAssets();
            _sequenceDifficulty = (Difficulty) (-1);
        }

        private void EventHandler()
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            // Get the mouse position
            Camera camera = SceneView.currentDrawingSceneView.camera;

            Vector2 mousePosition = Event.current.mousePosition;

            mousePosition.y = camera.pixelHeight - mousePosition.y;

            Vector3 worldPos = camera.ScreenToWorldPoint(mousePosition);
            Vector3 gridPos = _myTarget.WorldToGridCoordinates(worldPos);
            int col = (int)gridPos.x;
            int row = (int)gridPos.y;

            switch (_currentMode)
            {
                case Mode.Draw_Target_Sequence:
                    if (Event.current.type == EventType.MouseDown ||
                        Event.current.type == EventType.MouseDrag)
                    {
                        PaintPattern(col, row);
                    }
                    break;
                case Mode.Paint:
                    if (Event.current.type == EventType.MouseDown ||
                        Event.current.type == EventType.MouseDrag)
                    {
                        Paint(col, row);
                    }
                    break;
                case Mode.Edit:
                    if (Event.current.type == EventType.MouseDown)
                    {
                        Edit(col, row);
                        _originalPosX = col;
                        _originalPosY = row;
                    }
                    if (Event.current.type == EventType.Ignore ||
                        Event.current.type == EventType.MouseUp)
                    {
                        if (_itemInspected != null)
                        {
                            Move();
                        }
                    }
                    if (_itemInspected != null)
                    {
                        _itemInspected.transform.position = Handles.FreeMoveHandle(
                            _itemInspected.transform.position,
                            _itemInspected.transform.rotation,
                            GameData.GridSize / 2,
                            GameData.GridSize / 2 * Vector3.one,
                            Handles.RectangleCap);
                    }

                    break;
                case Mode.Erase:
                    if (Event.current.type == EventType.MouseDown ||
                         Event.current.type == EventType.MouseDrag)
                    {
                        Erase(col, row);
                    }
                    break;
                case Mode.View:
                default:
                    break;
            }

        }

        private void PaintPattern(int col, int row)
        {
            //check we're in bounds and have a piece selected
            if (!_myTarget.IsInsideGridBounds(col, row) || _pieceSelected == null || _targetIndex == -1)
                return;
            //check if we need to overwrite an existing piece
            if (_myTarget.GetTarget(col, row) != null)
            {
                DestroyImmediate(_myTarget.GetTarget(col, row).gameObject);
            }
            //do the paint
            GameObject obj = PrefabUtility.InstantiatePrefab(_pieceSelected.gameObject) as GameObject; //instance a new game object

            //check if we need to overwrite a target square
            for (int i = 0; i < _myTarget.TargetLayer.Length; i++)
            {
                if (_myTarget.TargetLayer[i] == null)
                    continue;
                if (_myTarget.TargetLayer[i].playerNo == _targetIndex)
                {
                    Target temp = _myTarget.TargetLayer[i];
                    _myTarget.TargetLayer[i] = null;
                    if (temp != null)
                    {
                        DestroyImmediate(temp.gameObject);
                    }

                }
            }
            obj.GetComponent<Target>().playerNo = _targetIndex;
            obj.GetComponent<SpriteRenderer>().color = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().PColours[_targetIndex];

            obj.transform.parent = _myTarget.transform;                                 // set it as a child to the target level
            obj.transform.name = string.Format("[{0},{1},{2}]", col, row, obj.name);    // where the object should end up
            obj.transform.position = _myTarget.GridToWorldCoordinates(col, row);        // get the right world position
            obj.hideFlags = HideFlags.HideInHierarchy;
            _myTarget.SetTarget(col, row, obj.GetComponent<Target>());                  // save a reference to it's level piece in the level
            _myTarget.GetTarget(col, row).gridPos = new Vector2(col, row);
        }



        private void Paint(int col, int row)
        {
            //check we're in bounds and have a piece selected
            if (!_myTarget.IsInsideGridBounds(col, row) || _pieceSelected == null)
                return;
            //check if we need to overwrite an existing piece
            if (_myTarget[col, row] != null)
            {
                DestroyImmediate(_myTarget[col, row].gameObject);
            }
            //do the paint
            GameObject obj = PrefabUtility.InstantiatePrefab(_pieceSelected.gameObject) as GameObject; //instance a new game object

            obj.transform.parent = _myTarget.transform;                                 // set it as a child to the target level
            obj.transform.name = string.Format("[{0},{1}] {2}", col, row, obj.name);    // where the object should end up
            obj.transform.position = _myTarget.GridToWorldCoordinates(col, row);        // get the right world position
            obj.hideFlags = HideFlags.HideInHierarchy;
            _myTarget[col, row] = obj.GetComponent<GridSquare>();                       // save a reference to it's level piece in the level
            _myTarget[col, row].gridPos = new Vector2(col, row);
        }
        private void Erase(int col, int row)
        {
            //check we're in bounds
            if (!_myTarget.IsInsideGridBounds(col, row))
                return;
            //Do Erase
            if (_myTarget[col, row] != null)
            {
                DestroyImmediate(_myTarget[col, row].gameObject);
            }

        }
        private void Edit(int col, int row)
        {
            //check we're in bounds and have a piece selected
            if (!_myTarget.IsInsideGridBounds(col, row) || _myTarget[col, row] == null)
            {
                _itemInspected = null;
            }
            else
            {
                _itemInspected = _myTarget[col, row].GetComponent<PaletteItem>() as PaletteItem;
            }
            Repaint();
        }

        private void Move()
        {
            Vector3 gridPoint = _myTarget.WorldToGridCoordinates(_itemInspected.transform.position);
            int col = (int)gridPoint.x;
            int row = (int)gridPoint.y;

            if (col == _originalPosX && row == _originalPosY)
            {
                return;
            }

            if (!_myTarget.IsInsideGridBounds(col, row) || _myTarget[col, row] != null)
            {
                _itemInspected.transform.position = _myTarget.GridToWorldCoordinates(_originalPosX, _originalPosY);
            }
            else
            {
                _myTarget[_originalPosX, _originalPosY] = null;
                _myTarget[col, row] = _itemInspected.GetComponent<GridSquare>();
                _myTarget[col, row].transform.position = _myTarget.GridToWorldCoordinates(col, row);
                _myTarget[col, row].gridPos = new Vector2(col, row);

            }



        }

        private void ModeHandler()
        {
            switch (_selectedMode)
            {
                case Mode.Draw_Target_Sequence:
                case Mode.Paint:
                case Mode.Edit:
                case Mode.Erase:
                    Tools.current = Tool.None;
                    break;
                case Mode.View:
                default:
                    Tools.current = Tool.View;
                    break;
            }
            if (_selectedMode != _currentMode)
            {
                _currentMode = _selectedMode;
                _pieceSelected = null;
                _itemInspected = null;
                Repaint();
            }

            SceneView.currentDrawingSceneView.in2DMode = true;
        }


        private void DrawPieceSelectedGUI()
        {
            EditorGUILayout.LabelField("Piece Selected", EditorStyles.boldLabel);
            if (_pieceSelected == null)
            {
                EditorGUILayout.HelpBox("No piece selected press F10 or click Tools-->Level Maker-->Show Palette to show the palette.", MessageType.Info);
            }
            else
            {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.LabelField(new GUIContent(_itemPreview), GUILayout.Height(40));
                EditorGUILayout.LabelField(_itemSelected.itemName);
                EditorGUILayout.EndVertical();
            }
        }

        private void DrawModeGUI()
        {
            List<Mode> modes = EditorUtils.GetListFromEnum<Mode>();
            List<string> modeLabels = new List<string>();
            foreach (Mode mode in modes)
                modeLabels.Add(mode.ToString());

            Handles.BeginGUI();
            GUILayout.BeginArea(new Rect(10, 10, 750, 40));

            _selectedMode = (Mode)GUILayout.Toolbar(
                (int)_currentMode,
                modeLabels.ToArray(),
                GUILayout.ExpandHeight(true),
                GUILayout.ExpandWidth(true));
            GUILayout.EndArea();
            Handles.EndGUI();
        }

        private void DrawLevelData()
        {
            EditorGUILayout.LabelField("Data", EditorStyles.boldLabel);
            

            _myTarget.Settings = (LevelSettings)EditorGUILayout.ObjectField(
                                    "Level Settings", _myTarget.Settings, typeof(LevelSettings));

            if (_myTarget.Settings != null)
            {
                Editor.CreateEditor(_myTarget.Settings).OnInspectorGUI();
            }
            else
            {
                EditorGUILayout.HelpBox("You must attach a LevelSettings asset!", MessageType.Warning);
            }
           
        }

        private void UpdateCurrentPieceInstance(PaletteItem item, Texture2D preview)
        {
            _itemSelected = item;
            _itemPreview = preview;
            _pieceSelected = (GridSquare)item.gameObject.GetComponent<GridSquare>();
            Repaint();
        }

        private void InitLevel()
        {
            int nullCount = 0;
            foreach (GridSquare gs in _myTarget.Grid)
            {
                if (gs == null)
                {
                    nullCount++;
                }
            }


            //check if there's any grid squares in the scene      
            _myTarget.GetGridSquares();
            _myTarget.GetTargetsInScene();

        }

    }
}
