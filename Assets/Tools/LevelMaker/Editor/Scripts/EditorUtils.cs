﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace LevelMaker
{
    public static class EditorUtils
    {
        public static void NewScene()
        {
            EditorApplication.SaveCurrentSceneIfUserWantsTo();
            EditorApplication.NewScene();
        }

        public static void CleanScene()
        {
            GameObject[] allObjs = Object.FindObjectsOfType<GameObject>();

            foreach (var go in allObjs)
            {
                GameObject.DestroyImmediate(go);
            }
        }
        public static void NewLevel()
        {
            NewScene();
            CleanScene();
            GameObject cam = Resources.Load("Prefabs/Camera") as GameObject;
            GameObject level = Resources.Load("Prefabs/Level") as GameObject;
            
            if (cam == null || level == null)
            {
                Debug.Log("NewLevel can't find a prefab object");
            }
            else
            {
                PrefabUtility.InstantiatePrefab(cam);
                PrefabUtility.InstantiatePrefab(level);

            }
        }

        public static T CreateAsset<T>(string path) where T : ScriptableObject
        {
            T dataClass = (T)ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(dataClass, path);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            
            return dataClass;
        }

        public static List<T> GetAssetsWithScript<T>(string path) where T : MonoBehaviour
        {
            T tmp;
            string assetPath;
            GameObject asset;

            List<T> assetList = new List<T>();
            //find all the prefabs in the path
            string[] guids = AssetDatabase.FindAssets("t:Prefab", new string[] { path });
            //loop through the returned list
            for (int i = 0; i < guids.Length; i++)
            {
                //get the path to the asset
                assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                //cast it as a gameobject into asset
                asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
                // try to get a reference to script from the asset 
                tmp = asset.GetComponent<T>();
                //if it does have the script we want add it to our list
                if (tmp != null)
                {
                    assetList.Add(tmp);
                }
            }
            return assetList;
        }

        public static List<T> GetListFromEnum<T>()
        {
            List<T> enumList = new List<T>();
            System.Array enums = System.Enum.GetValues(typeof(T));
            foreach (T e in enums)
            {
                enumList.Add(e);
            }
            return enumList;
        }

        public static void DisplayLevelMakerError(string dialogue, string ok)
        {
            EditorUtility.DisplayDialog("Level Maker Error",
                dialogue,
                ok);

            EditorApplication.Beep();
            Debug.DebugBreak();
            Debug.Break();
            EditorApplication.isPlaying = false;
        }

        public static void ShowList(SerializedProperty list)
        {
            //EditorGUILayout.PropertyField(list);
            EditorGUILayout.BeginVertical("box");

            if (list.arraySize == 0)
            {
                EditorGUILayout.HelpBox("There are no sequences! Go make some by clicking on Grid Stage in the hierarchy or click the button below to add a pre-made sequence.", MessageType.Info);
            }
            for (int i = 0; i < list.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                GUIContent content = new GUIContent("");
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i),content);
                if (GUILayout.Button("-"))
                {
                    int oldSize = list.arraySize;
                    list.DeleteArrayElementAtIndex(i);
                    if (list.arraySize == oldSize)
                    {
                        list.DeleteArrayElementAtIndex(i);
                    }
                }
                EditorGUILayout.EndHorizontal();

            }
            if (GUILayout.Button("+", GUILayout.ExpandWidth(true)))
            {
                list.InsertArrayElementAtIndex(0);
            }
            EditorGUILayout.EndVertical();

        }

        

    }
}
