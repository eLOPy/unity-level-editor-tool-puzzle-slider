﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace LevelMaker
{
    public class HelpWindow : EditorWindow
    {
        public static HelpWindow instance;
        bool _showIntro = true;
        bool _showNewLevel = false;
        bool _showGridBrush = false;
        bool _showSequenceEditor = false;
        bool _showDifficulty = false;
        bool _showControls = false;


        GUIStyle _paragraphStyle;

        Vector2 _scrollPos;
        public static void ShowHelp()
        {
            instance = (HelpWindow)EditorWindow.GetWindow(typeof(HelpWindow));
            instance.titleContent = new GUIContent("LevelMaker Help");
        }

        private void OnEnable()
        {
           
        }


        private void OnGUI()
        {
            _paragraphStyle = GUI.skin.textArea;
            _paragraphStyle.fontSize = 14;
            _scrollPos = GUILayout.BeginScrollView(_scrollPos);
            GUILayout.BeginVertical();
            GUIStyle contentsLabel = new GUIStyle( GUI.skin.label);

            contentsLabel.fontSize = 18;
            _showIntro = GUILayout.Toggle(_showIntro, "Introduction", contentsLabel);
            DrawIntroParagraph();
            _showNewLevel = GUILayout.Toggle(_showNewLevel, "Making A New Level.", contentsLabel);
            DrawHowtoMakeLevelParagraph();
            _showGridBrush = GUILayout.Toggle(_showGridBrush, "Painting Grid Squares.", contentsLabel);
            DrawHowToPaintParagraph();
            _showSequenceEditor = GUILayout.Toggle(_showSequenceEditor, "Making Target Sequences", contentsLabel);
            DrawHowToMakeTargetSequencesParagraph();
            _showDifficulty = GUILayout.Toggle(_showDifficulty, "Controlling The Difficulty Of Your Level.", contentsLabel);
            DrawHowToDifficultyParagraph();
            _showControls = GUILayout.Toggle(_showControls, "Game Controls.", contentsLabel);
            DrawControls();
            GUILayout.EndVertical();
            GUILayout.EndScrollView();
        }

        private void DrawIntroParagraph()
        {
            string text = "Hello this is the tool I've been working on...\n" +
                "It has 5 main features (Including this help window), you can click each sub-title to find out how to use each feature.";
            if (_showIntro)
            {
                GUILayout.BeginVertical();
                GUILayout.TextArea(text, _paragraphStyle);
                GUILayout.EndVertical();
            }
        }

        private void DrawHowtoMakeLevelParagraph()
        {
            string text =   "Go to Tools->Level Maker->New Level Scene.\n" +
                            "This will open the starting template scene for your level. Click it any time if you need to start over.\n" +
                            "In the Hierarchy find the 'Level Maker' GameObject inside the 'Level' and select it.\n" +
                            "Notice the message in the Inspector...\n" +
                            "Now go to Tools->Level Maker->New Level Settings and give it a name then save it in the project.\n" +
                            "Go back to the Inspector and add the level settings you just made.\n" +
                            "Now choose the time limit and the lengths of your level.\n" +
                            "You can re-use these settings across different levels.";
            if (_showNewLevel)
            {
                GUILayout.BeginVertical();
                GUILayout.TextArea(text, _paragraphStyle);
                GUILayout.EndVertical();
            }
        }

        private void DrawHowToPaintParagraph()
        {
            string text =   "Once you've made a settings asset, go to Tools->Level Maker->New Level Scene.\n" + 
                            
                            "\n"+
                            "While you have 'Level Maker' selected in the Hierarchy you can use the toolbar at to the top of the Scene View to edit the level.\n"+
                            "View:      -  Allows you to move the scene around like normal.\n"+
                            "Paint:     - Lets you paint on the grid whilst you have a piece selected:\n" +
                            "           - To select a piece press F10 or click on Tools->Level Maker->Palette to open the palette window and select a piece.\n"+
                            "           - To paint simply click any where on the grid whilst you have a Paint and a piece selected.\n"+
                            "Erase:     - Destroys pieces instead of painting them.\n"
                            ;
            if (_showGridBrush)
            {
                GUILayout.BeginVertical();
                GUILayout.TextArea(text, _paragraphStyle);
                GUILayout.EndVertical();
            }
        }

        private void DrawHowToMakeTargetSequencesParagraph()
        {
            string text =   "You may have noticed another button in the toolbar Draw_Target_Sequence.\n"+
                            "Click on this to make sequences of targets for your level.\n"
                            +"Select which player target you want to draw and paint like normal.\n" +
                            "Once your happy positioning it click 'Add Pattern Frame to Sequence' to stage it.\n"+
                            "You can have maximum 5 frames.\n" +
                            "If you screw up or aren't happy use the red clear button to start over.\n"+
                            "When you're finished making the sequence click 'Save Sequence to Level' and select a difficulty,\n" +
                            "Unity will then prompt you for a save location for an asset file and will add the new sequence to the level.";
            if (_showSequenceEditor)
            {
                GUILayout.BeginVertical();
                GUILayout.TextArea(text, _paragraphStyle);
                GUILayout.EndVertical();
            }
        }

        private void DrawHowToDifficultyParagraph()
        {
            string text =   "After you've made a enough sequences of varying difficulty you'll probably want to control at what point they appear in the game\n"
                +           "Select 'Difficulty Editor' in the Hierarchy Inspector and click on the curve.\n"+
                            "Use this curve to control the difficulty (y - axis) in the game over time (x-axis).\n"+
                            "You can also modify which sequences the level will use.";
            if (_showDifficulty)
            {
                GUILayout.BeginVertical();
                GUILayout.TextArea(text, _paragraphStyle);
                GUILayout.EndVertical();
            }
        }

        private void DrawControls()
        {
            string text =   "Use 1 and 2 to Hold circles.\n" +
                            "Move the circles with the arrow keys.\n"+
                            "Aim of the game: Move the circles on to the X's.";
            if (_showControls)
            {
                GUILayout.BeginVertical();
                GUILayout.TextArea(text, _paragraphStyle);
                GUILayout.EndVertical();
            }
        }



    }
}