﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace LevelMaker
{
    public static class MenuItems
    {
        [MenuItem("Tools/Level Maker/New Level Scene")]
        private static void NewLevel()
        {
            EditorUtils.NewLevel();
        }

        [MenuItem("Tools/Level Maker/Show Palette _F10")]
        private static void ShowPalette()
        {
            PaletteWindow.ShowPalette();
        }


        [MenuItem("Tools/Level Maker/New Level Settings")]
        private static void NewLevelSettings()
        {
            string path = EditorUtility.SaveFilePanelInProject(
                "New Level Settings",
                "LevelSettings",
                "asset",
                "Define the name for the LevelSettings asset"
                );
            if (path != "")
            {
                EditorUtils.CreateAsset<LevelSettings>(path);
            }
        }

        [MenuItem("Tools/Level Maker/Help")]
        private static void Help()
        {
            HelpWindow.ShowHelp();
        }

        //[MenuItem("Tools/Level Maker/New Target Pattern")]
        //private static void NewTargetPattern()
        //{
        //    string path = EditorUtility.SaveFilePanelInProject(
        //        "New Target Pattern",
        //        "TargetPattern",
        //        "asset",
        //        "Define the name for the TargetPattern asset"
        //        );
        //    if (path != "")
        //    {
        //        //EditorUtils.CreateAsset<TargetFrame>(path);
        //    }
        //}
    }
}
