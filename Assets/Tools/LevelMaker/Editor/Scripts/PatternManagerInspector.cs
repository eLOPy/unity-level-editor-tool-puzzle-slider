﻿using UnityEngine;
using UnityEditor;

using System.Collections;

namespace LevelMaker
{
    [CustomEditor(typeof(PatternManager))]
    public class PatternManagerInspector : Editor
    {

        PatternManager _myTarget;

        void OnEnable()
        {
            _myTarget = (PatternManager)target;
        }

        void OnDisable()
        {

        }

        void OnDestroy()
        {

        }


        public override void OnInspectorGUI()
        {


            EditorGUILayout.BeginVertical("box");
            GUIContent curveLabel = new GUIContent("Difficulty Curve:", "Controls how difficult (i.e. which target sequences are used) the game gets over time.");
            EditorGUILayout.LabelField(curveLabel, EditorStyles.boldLabel);
            _myTarget._difficultyCurve = EditorGUILayout.CurveField(_myTarget._difficultyCurve, GUILayout.MinHeight(100));

            DrawSequencesGUI();

            EditorGUILayout.EndVertical();

            if (UnityEngine.GUI.changed)
            {
                EditorUtility.SetDirty(_myTarget);
            }


        }

        private void DrawSequencesGUI()
        {
            EditorGUILayout.LabelField("Sequences:", EditorStyles.boldLabel);
            EditorUtils.ShowList(serializedObject.FindProperty("_sequences"));

            serializedObject.ApplyModifiedProperties();

        }
        void OnSceneGUI()
        {
            //Handles.BeginGUI();
            //GUILayout.BeginArea(new Rect(Screen.width / 2, Screen.height / 2, 500, 500));



            //GUILayout.EndArea();
            //Handles.EndGUI();
        }

    }

}