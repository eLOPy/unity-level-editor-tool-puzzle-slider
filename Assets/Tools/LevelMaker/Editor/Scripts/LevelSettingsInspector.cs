﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace LevelMaker
{
    [CustomEditor(typeof(LevelSettings))]
    public class LevelSettingsInspector : Editor
    {

        LevelSettings _myTarget;
        void OnEnable()
        {
            _myTarget = (LevelSettings)target;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical("box");

            GUIStyle a = new GUIStyle(EditorStyles.boldLabel);
            a.fontSize = 14;
            a.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label("Properties", a);
            _myTarget.iWidth = DrawCustomIntField("Width", _myTarget.iWidth);//;
            _myTarget.iHeight = DrawCustomIntField("Height", _myTarget.iHeight);
            _myTarget.sTimeLimit = System.Math.Max(0, EditorGUILayout.IntField("Time", _myTarget.sTimeLimit));
            EditorGUILayout.EndVertical();

            if (UnityEngine.GUI.changed)
            {
                EditorUtility.SetDirty(_myTarget);
            }
        }

        private int DrawCustomIntField(string s, int p)
        {
            GUILayout.BeginHorizontal();
            System.Math.Max(0, EditorGUILayout.IntField(s, p));
            if (GUILayout.Button("-"))
            {
                if (p > 0)
                    p--;
            }
            if (GUILayout.Button("+"))
            {
                p++;
            }

            GUILayout.EndHorizontal();
            return p;
        }
    }
}
